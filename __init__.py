#!/usr/bin/env python3

import sys
import logging
from typing import IO, Optional

if __name__ == '__main__':
  from sparse import sparse
  from struct import struct
else:
  from .sparse import sparse
  from .struct import struct



ellipsis_type = type(...)


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(stream=sys.stderr)
handler.setFormatter(logging.Formatter(fmt='〈〈〈 HarryPyther 〉〉〉 %(message)s'))
logger.addHandler(handler)




class HarryPyther(object):
  """HarryPyther class
  Iterator to get data from a GCD-McS’s *.eventdata file event by event.
  Compatible with GCD-McS version 17 (Umbridge).
  Not compatible with version 16 (Delacour).
  
  Usage:
      from HarryPyther import HarryPyther
      
      with HarryPyther('path/to/file.eventdata') as hp:
          do_some_initialization()
          while hp.next():
              do_something(hp)
      
      or
      
      for hp in HarryPyther('path/to/file.eventdata'):
          do_something(hp)
  """
  
  def __init__(self, file = 'default.eventdata', nevents = None, ofile = None) -> None:
    """Constructor.
    Args:
        file    (str):  An *.eventdata file to process.
        nevents (int):  If set, restrict the number of events to be processed.
                        If None, go on until end of file.
        ofile   (str):  Output *.eventdata file to be created (rewritten if exists).
                        If set, act as a filter.  Events are copied to the new file
                        whenever the `flush` method is called.
    """
    self._filename = file
    self._file = None
    self._nevents = nevents
    self.use_ScintStrip_PMSignals = False
    self._buffer = ''
    self._ofile = open(ofile, 'w')  if ofile is not None  else None
  
  def __enter__(self) -> 'HarryPyther':
    """Enter a `with` statement.
    This opens the input file ang reads geometry.
    """
    if (self._file is None) or (self._file.closed):
      self._file = open(self._filename, 'r')
    self.read_geometry()
    self.event = 0
    return self
  
  def __exit__(self, type, value, traceback) -> None:
    """Exit a `with` statement.
    This closes the input file.
    """
    self.close()
  
  def __del__(self) -> None:
    """Destructor
    Calls the `close` method.
    """
    self.close()
  
  def close(self) -> None:
    """Finalize input and output files"""
    if self._file is not None:
      self._file.close()
      self._file = None
    if self._ofile is not None:
      self._ofile.write('\n')
      self._ofile.close()
      self._ofile = None
    
  def flush(self) -> None:
    """Write current event to the output file (if any specified).
    """
    if self._ofile is not None:
      self._ofile.write(self._buffer)
    self._buffer = ''
  
  def read_geometry(self) -> None:
    """Reads geometry header from input *.eventdata file.
    This must be done exactly once before reading events.
    It is unlikely that you would have to use it explicitly, though:
    this method is called automatically when using
    recomended `with` or `for` interface.
    """
    import re
    self.header = self.readline()
    self.version = self.header.lstrip('# Created by ').strip()
    search = re.search(r'\bv([\d.]+)\b', self.version)
    self.version_name = re.search(r'\(([^)]+)\)', self.version).group(1)
    if search is None:
      raise NoVersionException(self.header)
    self.version_number_string = search.group(1)
    version_match = re.search(r'^(\d+)\.(\d+)', self.version_number_string)
    if version_match is None:
      raise WrongFormatException(f'Version number “{self.version_number_string}” is not matched')
    self.version_major = int(version_match.group(1))
    self.version_minor = int(version_match.group(2))
    self.version_number = float(self.version_number_string)
    
    self._AC      = self.ACSystem(self, 'AC-top')
    self._leftAC  = self.ACSystem(self, 'AC-left')
    self._frontAC = self.ACSystem(self, 'AC-front')
    self._rightAC = self.ACSystem(self, 'AC-right')
    self._backAC  = self.ACSystem(self, 'AC-back')
    self._C       = self.StripSystem(self, 'C')
    self._S1      = self.ToFSystem(self, 'S1')
    self._S2      = self.ToFSystem(self, 'S2')
    self._CC1     = self.StripSystem(self, 'CC1')
    self._CC1calo = self.CC1ScintSystem(self, 'CC1')
    self._S3      = self.ScintSystem(self, 'S3')
    self._leftLD  = self.ScintSystem(self, 'LD-left')
    self._frontLD = self.ScintSystem(self, 'LD-front')
    self._rightLD = self.ScintSystem(self, 'LD-right')
    self._backLD  = self.ScintSystem(self, 'LD-back')
    self._CC2     = self.CaloSystem(self, 'CC2')
    self._S4      = self.ScintSystem(self, 'S4')
    self.readline()
    self._geometry = self._buffer
    self.flush()
  
  def write_geometry(self, stream: Optional[IO] = None) -> None:
    if stream is None:
      stream = self._ofile
    stream.write(self._geometry)
  
  def write_event(self, stream: Optional[IO] = None) -> None:
    if stream is None:
      stream = self._ofile
    self._C.write(stream)
    print('', file=stream)
    self._CC1.write(stream)
    print('', file=stream)
    self._AC.write(stream)
    self._leftAC.write(stream)
    self._frontAC.write(stream)
    self._rightAC.write(stream)
    self._backAC.write(stream)
    self._S1.write(stream)
    self._S2.write(stream)
    self._S3.write(stream)
    self._S4.write(stream)
    self._CC1calo.write(stream)
    self._leftLD.write(stream)
    self._frontLD.write(stream)
    self._rightLD.write(stream)
    self._backLD.write(stream)
    print('', file=stream)
    self._CC2.write(stream)
    print('\n', file=stream)
  
  def __iter__(self) -> 'HarryPyther':
    """Turn a HarryPyther object to an iterator.
    In fact, this initializes the object via `__enter__` and returns itself.
    """
    self.__enter__()
    return self
  
  def __next__(self, use_iterator: bool = True):
    """Increment an iterator to the next event.
    Returns `self`, so an ‘element’ of this ‘collection of events’
    is the HarryPyther object itself.
    """
    try:
      self._buffer = ''
      self.iter = use_iterator
      if self._nevents is not None and self.event >= self._nevents:
        return self._raise_stop(use_iterator)
      def skipline():
        if self.readline() == '':
          return self._raise_stop(use_iterator)
      assert self._file is not None
      pos = self._file.tell()
      self._file.readline()
      self._file.seek(pos)
      self.event += 1
      if not self._C      .read(self): return False
      if not self._CC1    .read(self): return False
      if not self._AC     .read(self): return False
      if not self._leftAC .read(self): return False
      if not self._frontAC.read(self): return False
      if not self._rightAC.read(self): return False
      if not self._backAC .read(self): return False
      if not self._S1     .read(self): return False
      if not self._S2     .read(self): return False
      if not self._S3     .read(self): return False
      if not self._S4     .read(self): return False
      if not self._CC1calo.read(self): return False
      if not self._leftLD .read(self): return False
      if not self._frontLD.read(self): return False
      if not self._rightLD.read(self): return False
      if not self._backLD .read(self): return False
      skipline()
      if not self._CC2    .read(self): return False
      skipline(); skipline()
      return self
    except KeyboardInterrupt:
      if use_iterator:
        raise StopIteration
      else:
        raise
  
  def next(self):
    """Proceed to the next event.
    Unlike `__next__` magic method, returns False when iteration
    is meant to be stopped (instead of raising `StopIteration` exception).
    This allows using `while hp.next()` loop.
    """
    return self.__next__(use_iterator=False)
  
  def _raise_stop(self, use_iterator: bool = True) -> bool:
    self.close()
    if use_iterator:
      raise StopIteration()
    return False
  
  def readline(self) -> str:
    assert self._file is not None
    line = self._file.readline()
    if line == '':
      raise StopIteration()
    self._buffer += line
    return line



  class System(object):
    """Astract class representing a detecting system.
    Contains geometry and event information.
    
    A number of systems can be accessed through HarryPyther’s properties:
        topAC, C, S1, CC2, etc.
    
    The data is contained in tha `data` attribute and can be accessed with it,
    but it is safer to use special `get_*()` and `is_*()` methods of a specific system.
    """
    from abc import abstractmethod
    
    @abstractmethod
    def __init__(self, harry: 'HarryPyther'):
      "Read geometry"
      self.data = harry.Data()
      self.modified = False
      self._string = ''
    
    @abstractmethod
    def read(self, harry: 'HarryPyther'):
      "Read next event"
      pass
  
  
  
  class ScintSystem(System):
    """Scintillation system object.
    This represent several layers, each one consisting of a number of
    position- and time-sensitive strips.
    """
    
    def __init__(self, harry: 'HarryPyther', expected_name = None):
      harry.System.__init__(self, harry)
      
      # read header: AC-top  nlayers
      line = harry.readline()
      if line == '': return harry._raise_stop(harry.iter)
      self.name, self.nlayers = maps(line.split(), str, int)
      if expected_name is not None and self.name != expected_name:
        raise WrongFormatException(f'Line {cropline(line)} must have the first token {expected_name}')
      
      # prepare arrays
      self.X = []; self.Y = []; self.Z = [];  self.dX = []; self.dY = []; self.dZ = []
      self.strip_axis = []; self.along_axis = []; self.tmin = []; self.tmax = []
      self.tbins = []; self.pbins = []; self.sbins = []; self.soffset = []
      self._default_p = []; self._default_tp = []; self._default_stp = []; self._default_s = []
      
      for ilayer in range(self.nlayers):
        line = harry.readline()
        maps_append(line.split(),
            float,self.X, float,self.Y, float,self.Z, float,self.dX, float,self.dY, float,self.dZ,
            str,self.strip_axis, str,self.along_axis,
            float,self.tmin, float,self.tmax,
            int,self.tbins, int,self.pbins, int,self.sbins,
            float,self.soffset)
        self._default_p = sparse({}, self.pbins[ilayer], default = 0.);
        self._default_tp = sparse({}, self.tbins[ilayer], default = self._default_p);
        self._default_stp = sparse({}, self.sbins[ilayer], default = self._default_tp);
        self._default_s = sparse({}, self.sbins[ilayer], default = 0.)
        # self._default_p.append(sparse({}, self.pbins[ilayer], default = 0.));
        # self._default_tp.append(sparse({}, self.tbins[ilayer], default = self._default_p));
        # self._default_stp.append(sparse({}, self.sbins[ilayer], default = self._default_tp));
        # self._default_s.append(sparse({}, self.sbins[ilayer], default = 0.))
        #self._default_p[ilayer].total = [0.] * self.nlayers;
        #self._default_tp[ilayer].total = [0.] * self.nlayers;
        #self._default_stp[ilayer].total = [0.] * self.nlayers;
    
    def read(self, harry: 'HarryPyther') -> bool:
      self._string = ''
      # prepare arrays
      if harry.use_ScintStrip_PMSignals:
        self.data.times = []; self.data.edeps = []; self.data.strips = []
      else:
        self.data.tmin = []; self.data.tmax = []; self.data.etot = []
        self.data.array = []; self.data.strips = []; self.data.nstrips = []
      
      for ilayer in range(self.nlayers):
        line = harry.readline()
        self._string += line
        words = line.split()
        if self.name is not None and self.name != words[0]:
          raise WrongFormatException(f'Line {cropline(line)} must have the first token {self.name}')
        words = words[1:]
        
        if harry.use_ScintStrip_PMSignals:
          pairs = listmap(float, words)
          self.data.times.append(pairs[0::2])
          self.data.edeps.append(pairs[1::2])
          if not self.sbins == len(self.data.times) == len(self.data.edeps):
            harry._raise_stop(harry.iter)
          class TE:
            def __init__(self, te):
              self.tmin = te[0]
              self.edep = te[1]
          self.data.strips.append(listmap(TE, zip(self.data.times, self.data.edeps)))
        else:
          maps_append(words[: 3], float,self.data.tmin, float,self.data.tmax, float,self.data.etot)
          self.data.array.append(self._default_stp.clone())
          self.data.strips.append(self._default_s.clone())
          w = 3
          Ns = int(words[w]); w += 1
          self.data.nstrips.append(Ns)
          for js in range(Ns):
            s = int(words[w]); w += 1
            Es = float(words[w]); w += 1
            #print('s =', s, '; Es =', Es)
            slice_s = self.data.array[ilayer][s] = self._default_tp.clone()
            self.data.strips[ilayer][s] = slice_s.total = Es
            Nt = int(words[w]); w += 1
            for jt in range(Nt):
              t = int(words[w]); w += 1
              #print('t =', t)
              Et = float(words[w]); w += 1
              slice_st = slice_s[t] = self._default_p.clone()
              slice_st.total = Et
              #slice_st.resize(100)
              Np = int(words[w]); w += 1
              for jp in range(Np):
                p = int(words[w]); w += 1
                #print('p =', p)
                Ep = float(words[w]); w += 1
                slice_stp = slice_st[p] = Ep
      return True
    
    def write(self, stream: IO):
      if self.modified:
        data = self.data
        for ilayer in range(self.nlayers):
          nstrips_triggered = data.nstrips[ilayer]
          stream.write(f'{self.name} {data.tmin[ilayer]} {data.tmax[ilayer]} {data.etot[ilayer]} {nstrips_triggered} ')
          strip_etots = data.strips[ilayer]
          s_array = data.array[ilayer]
          for s_bin in data.strips[ilayer].indices():
            stream.write(f'{s_bin} {strip_etots[s_bin]} {len_stored(s_array[s_bin])} ')
            t_array = s_array[s_bin]
            for t_bin in t_array.indices():
              stream.write(f'{t_bin} {t_array[t_bin].total} {len_stored(t_array[t_bin])} ')
              p_array = t_array[t_bin]
              for p_bin in p_array.indices():
                stream.write(f'{p_bin} {p_array[p_bin]} ')
                t_array = s_array[s_bin]
          stream.write('\n')
      else:
        print(self._string, file=stream, end='')
    
    
    def is_triggered_layer(self, ilayer: int, threshold = 0) -> bool:
      """True if layer is triggered.
      A layer is triggered if it has a strip that has energy deposit above threshold.
      """
      if threshold == 0:
        return self.data.nstrips[ilayer] > 0
      layer = self.data.strips[ilayer]
      return any(dE > threshold for dE in layer)
    
    def is_triggered(self, threshold = 0) -> bool:
      """True if at least one layer is triggered.
      A layer is triggered if it has a strip that has energy deposit above threshold.
      """
      if threshold == 0:
        return any(map(lambda x: x > 0, self.data.nstrips))
      return any(dE > threshold for layer in self.data.strips for dE in layer)
    is_triggered_any_layer = is_triggered
    
    def is_triggered_all_layers(self, threshold = 0) -> bool:
      """True if all layers are triggered.
      A layer is triggered if it has a strip that has energy deposit above threshold.
      """
      if threshold == 0:
        return all(map(lambda x: x > 0, self.data.nstrips))
      return all(
        self.is_triggered_layer(ilayer, threshold)
        for ilayer, layer in enumerate(self.data.strips)
      )
    
    def is_triggered_same_strip(self, threshold=0) -> bool:
      """True if the same strip in all layers is triggered.
      A layer is triggered if it has a strip that has energy deposit above threshold.
      """
      nlayers = self.nlayers
      nstrips = self.sbins[0]
      triggered_strips = [None] * nlayers

      # fill triggered_strips
      for ilayer, layer in enumerate(self.data.strips):
        triggered_strips[ilayer] = [False] * nstrips
        for istrip, dE in enumerate(layer):
          if dE > threshold:
            triggered_strips[ilayer][istrip] = True
      #print(triggered_strips)

      # check the same strip
      for istrip in range(nstrips):
        this_strip_triggered = all(
          triggered_strips[ilayer][istrip]
          for ilayer in range(nlayers)
        )
        if this_strip_triggered:
          return True

      return False
    
    def get_array(self, sparse: bool = True) -> sparse | list[list[list[float]]]:
      """List of a 3D matrix (indices [s,t,p]) of energy deposits.
      A 3D matrix is represented as a nested sparse array or
      a nested list, depending on the value of 'sparse' argument.
      """
      if sparse:
        return self.data.array
      else:
        return [[[p for p in t] for t in s] for s in self.data.array]
    
    def get_strips(self, sparse: bool = True) -> sparse | list[float]:
      """List of an array of energy deposits by strips.
      An array is represented as a sparse array or
      a list, depending on the value of 'sparse' argument.
      """
      if sparse:
        return self.data.strips
      else:
        return list(self.data.array)
    
    def get_etot(self, layer: Optional[int] = None) -> float:
      """Total energy deposit in the specified layer (0-based number)
      or total in the whole detector (if layer is None).
      """
      if layer is None:
        return sum(self.data.etot)
      else:
        return self.data.etot[layer]
    
    def get_t_bounds(self, layer: Optional[int] = None) -> tuple[float|list[float], float|list[float], float|list[float]]:
      """Return (tmin, tmax, tbins) tuple of a time histogram bounds.
      Each element is a number for the specified layer
      or a list through layers (if layer is None).
      """
      if layer is None:
        return self.tmin, self.tmax, self.tbins
      else:
        return self.tmin[layer], self.tmax[layer], self.tbins[layer]
    
    def get_t_range(self, layer: Optional[int] = None) -> tuple[float, float]:
      """Return (tmin, tmax) tuple of an ealiest and a latest hit
      in the specified layer or in the whole detector (if layer is None).
      """
      if layer is None:
        return min(self.data.tmin), max(self.data.tmax)
      else:
        return self.data.tmin[layer], self.data.tmax[layer]
    
    def get_size(self, layer: Optional[int] = None) -> tuple[float|list[float], float|list[float], float|list[float]]:
      """Return total sizes (X, Y, Z) tuple of the specified layer (0-based number)
      or a list through layers (if layer is None).
      """
      if layer is None:
        return self.dX, self.dY, self.dZ
      else:
        return self.dX[layer], self.dY[layer], self.dZ[layer]
    
    def get_strip_offset(self, layer: Optional[int] = None) -> float|list[float]:
      """Return strip offset of the specified layer (0-based number)
      or a list through layers (if layer is None).
      """
      if layer is None:
        return self.soffset
      else:
        return self.soffset[layer]
    
    def get_nstrips(self, layer: Optional[int] = None) -> float|list[float]:
      """List of numbers of strips in the specified layer (0-based number)
      or a list through layers (if layer is None).
      """
      return self.sbins if layer is None else self.sbins[layer]
  
  
  class ACSystem(ScintSystem):
    "ScintSystem specification"
  class ToFSystem(ScintSystem):
    "ScintSystem specification"
  class CC1ScintSystem(ScintSystem):
    "ScintSystem specification"
  
  
  class StripSystem(System):
    """Strip system object.
    This represent several layers, each one consisting of
    a number of strips.
    """
    
    def __init__(self, harry: 'HarryPyther', expected_name: Optional[str] = None):
      harry.System.__init__(self, harry)
      line = harry.readline()
      if line in ('', '\n'): return harry._raise_stop(harry.iter)
      self.name, self.nlayers, self.width, self.pitch = maps(line.split(), str, int, float, float)
      if expected_name is not None and self.name != expected_name:
        raise WrongFormatException(f'Line {cropline(line)} must have the first token {expected_name}')

      self.nstrips = round(self.width / self.pitch)
      self.layersY = []
      self.layersZ = []
      for ilayer in range(self.nlayers):
        line = harry.readline()
        X, dX = listmap(float, line.split())
        self.layersY.append(struct(X = X, dX = dX))
        line = harry.readline()
        X, dX = listmap(float, line.split())
        self.layersZ.append(struct(X = X, dX = dX))
      self.data.Y = struct()
      self.data.Z = struct()
    
    def read(self, harry: 'HarryPyther') -> bool:
      self._string = ''
      line = harry.readline()
      self._string = line
      if line in ('', '\n'): return harry._raise_stop(harry.iter)
      name, = maps(line.split(), str)
      if self.name is not None and self.name != name:
        raise WrongFormatException(f'Line {cropline(line)} must have the first token {self.name}')
      self.data.Y.nstrips = [0] * self.nlayers
      self.data.Y.stripno = [None] * self.nlayers
      self.data.Y.edep = [None] * self.nlayers
      self.data.Z.nstrips = [0] * self.nlayers
      self.data.Z.stripno = [None] * self.nlayers
      self.data.Z.edep = [None] * self.nlayers
      for ilayer in range(self.nlayers):
        line = harry.readline()
        self._string += line
        if line in ('', '\n'): return harry._raise_stop(harry.iter)
        words = line.split()
        self.data.Y.nstrips[ilayer] = int(words[0])
        self.data.Y.stripno[ilayer] = listmap(int, words[1 :: 2])
        self.data.Y.edep[ilayer]  = listmap(float, words[2 :: 2])
        if len(self.data.Y.stripno) != len(self.data.Y.edep):
          raise WrongFormatException(f'Line {cropline(line)} must contain strip data and is corrupted')
        line = harry.readline()
        self._string += line
        words = line.split()
        self.data.Z.nstrips[ilayer] = int(words[0])
        self.data.Z.stripno[ilayer] = listmap(int, words[1 :: 2])
        self.data.Z.edep[ilayer]  = listmap(float, words[2 :: 2])
        if len(self.data.Z.stripno) != len(self.data.Z.edep):
          raise WrongFormatException(
              f'Line {cropline(line)} must contain strip data and is corrupted')
      if harry.readline() != '\n':
        raise WrongFormatException(f'Line {cropline(line)} must be empty')
      return True
    
    def write(self, stream: IO) -> None:
      if self.modified:
        print(self.name, file=stream)
        for ilayer in range(self.nlayers):
          sparr = self._get_edep_sparse(ilayer)
          
          pairs = sparr.Y._pairs
          write_string(stream, len(pairs))
          for stripNo in sorted(pairs):
            write_string(stream, stripNo, pairs[stripNo])
          print('', file=stream)
          
          pairs = sparr.Z._pairs
          write_string(stream, len(pairs))
          for stripNo in sorted(pairs):
            write_string(stream, stripNo, pairs[stripNo])
          print('', file=stream)
      else:
        print(self._string, file=stream, end='')
    
    def get_n_triggered(self, layer: Optional[int] = None) -> struct | list[struct]:
      """Number of triggered strips in the given `layer`.
      Return a struct with `X` and `Y` attributes.
      If `layer` is None, return a list of such structs by layers.
      """
      if layer is not None:
        return struct(Y = self.data.Y.nstrips[layer], Z = self.data.Z.nstrips[layer])
      else:
        return [
          struct(Y = self.data.Y.nstrips[i], Z = self.data.Z.nstrips[i])
          for i in range(self.nlayers)
        ]
    
    def is_triggered(self, layer: Optional[int] = None) -> bool:
      """True if at least one layer is triggered.
      If `layer` is given, return a struct with `X` and `Y` attributes.
      """
      if layer is not None:
        return struct(Y = self.data.Y.nstrips[layer] > 0, Z = self.data.Z.nstrips[layer] > 0)
      else:
        for nstrips in self.data.Y.nstrips:
          if nstrips > 0:
            return True
        for nstrips in self.data.Z.nstrips:
          if nstrips > 0:
            return True
        return False
    
    def get_edep_Y(self, layer: int, strip: int) -> float:
      try:
        stripno = self.data.Y.stripno[layer].index(strip)
        return self.data.Y.edep[layer][stripno]
      except ValueError:
        return 0.
    def get_edep_Z(self, layer: int, strip: int) -> float:
      try:
        stripno = self.data.Z.stripno[layer].index(strip)
        return self.data.Z.edep[layer][stripno]
      except ValueError:
        return 0.
    
    def get_edep(self, layer: Optional[int] = None, sparse: bool = True) -> struct | list[struct]:
      """Struct of energy deposits with Y and Z attributes.
      Each struct element is a list (if not `sparse`)
      or a sparse array (if `sparse`).
      List of structs if `layer` is None.
      """
      if layer is None:
        return [self.get_edep(layer, sparse) for layer in range(self.nlayers)]
      return self._get_edep_sparse(layer) if sparse else self._get_edep_array(layer)
    
    def _get_edep_sparse(self, layer: int) -> struct:
      y = zip(self.data.Y.stripno[layer], self.data.Y.edep[layer])
      z = zip(self.data.Z.stripno[layer], self.data.Z.edep[layer])
      return struct(Y = sparse(dict(y), self.nstrips, 0.), Z = sparse(dict(z), self.nstrips, 0.))
    
    def _get_edep_array(self, layer: int) -> struct:
      sp = self._get_edep_sparse(layer)
      return struct(Y = list(sp.Y), Z = list(sp.Z))
    
    def get_etot(self, layer: Optional[int] = None) -> float | struct:
      """Total energy deposit in the layer; a struct with Y and Z attributes.
      If `layer` is None, return total energy in the whole detector."""
      if layer is None:
        return sum( sum(edep) for edep in self.data.Y.edep ) + sum( sum(edep) for edep in self.data.Z.edep )
      else:
        return struct(Y = sum(self.data.Y.edep[layer]), Z = sum(self.data.Z.edep[layer]))
  
  
  class EnergySystem(System):
    """Outdated"""
    def __init__(self, harry: 'HarryPyther', geo, expected_name: Optional[str] = None):
      harry.System.__init__(self, harry)
      self.name = expected_name # no check here
      self.geo = geo
      self.layers = []
      for ilayer in range(self.geo.nlayers):
        # <Pettigrew>
        line = harry.readline()
        X, dX = listmap(float, line.split())
        self.layers.append(struct(X = X, dX = dX))
        # </Pettigrew>
    
    
    def read(self, harry: 'HarryPyther'):
      line = harry.readline()
      self._string = line
      words = line.split()
      name = words[0]
      if self.name is not None and self.name != name:
        raise WrongFormatException(f'Line {cropline(line)} must have the first token {self.name}')
      nlayers = self.geo.nlayers
      words = words[1:]
      etot = listmap(float, words)
      self.data.etot = etot
      self.data.edettot = sum(etot)
      return True
    
    def write(self, stream: IO):
      if self.modified:
        raise NotImplementedError('EnergySystem.write')
      else:
        print(self._string, file=stream, end='')
    
    def get_edep(self, layer: Optional[int] = None, sparse: bool = True):
      """Energy deposit in the layer.
      List of deposits if `layer` is None.
      """
      if layer is None:
        return self.data.etot
      else:
        return self.data.etot[layer]
    
    def get_etot(self):
      "Total energy deposit in the detector."
      return self.data.edettot
  
  
  class CaloSystem(System):
    def __init__(self, harry: 'HarryPyther', expected_name: Optional[str] = None):
      harry.System.__init__(self, harry)
      line = harry.readline()
      if line == '': return harry._raise_stop(harry.iter)
      words = line.split()
      self.name = words[0]
      if expected_name is not None and self.name != expected_name:
        raise WrongFormatException(f'Line {cropline(line)} must have the first token {expected_name}')
      words = words[1:]
      if words[0] == '+':
        self.expanded = True
        n = self.nlayers = int(words[1])
        nY = self.nitemsY = listmap(int, words[2 : n + 2])
        nZ = self.nitemsZ = listmap(int, words[n + 2 : 2*n + 2])
        N = sum(nY[i] * nZ[i] for i in range(n))
        coords = listmap(float, words[2*n + 2 :])
        self.pos = {}
        self.dims = {}
        # for i in range(N):
        #   self.pos.append(coords[6*i : 6*i + 3])
        #   self.dims.append(coords[6*i + 3 : 6*i + 6])
        i = 0
        for x in range(n):
          for y in range(nY[x]):
            for z in range(nZ[x]):
              self.pos[(x, y, z)] = coords[6*i : 6*i + 3]
              self.dims[(x, y, z)] = coords[6*i + 3 : 6*i + 6]
              i += 1
      elif words[0] == '-':
        self.expanded = False
        self.X, self.dX = listmap(float, line[1:].split())
      else:
        raise WrongFormatException(f'Line {cropline(line)} must begin with either + or -')
    
    def read(self, harry: 'HarryPyther'):
      from collections import defaultdict
      line = harry.readline()
      self._string = line
      words = line.split()
      name = words[0]
      if self.name is not None and self.name != name:
        raise WrongFormatException(f'Line {cropline(line)} must have the first token {self.name}')
      words = words[1:]
      self.data.etot = float(words[0])
      if   words[1] == '+' and self.expanded:
        nx = int(words[2])
        iword = 3
        self.data.edep_dict = defaultdict(lambda: 0.0)
        for ix in range(nx):
          x = int(words[iword]); iword += 1
          ny = int(words[iword]); iword += 1
          for iy in range(ny):
            y = int(words[iword]); iword += 1
            nz = int(words[iword]); iword += 1
            for iz in range(nz):
              z = int(words[iword]); iword += 1
              edep = float(words[iword]); iword += 1
              self.data.edep_dict[(x, y, z)] = edep
      elif words[1] == '-' and not self.expanded:
        pass
      else:
        raise WrongFormatException(f'Line {cropline(line)} must have the second token {"+" if self.expanded else "-"}')
      return True
      # TODO: scan scint items, fill self.data.edep
    
    def write(self, stream: IO):
      if self.modified:
        data = self.data
        layers = sorted({index[0] for index in data.edep_dict})
        nlayers = len(layers)
        stream.write(f'{self.name} {data.etot} + {nlayers} ')
        for x in layers:
          layer = {ind: val for ind, val in data.edep_dict.items() if ind[0] == x}
          ys = sorted({index[1] for index in layer})
          stream.write(f'{x} {len(ys)} ')
          for y in ys:
            yslice = {ind: val for ind, val in layer.items() if ind[1] == y}
            zs = sorted({index[2] for index in yslice})
            stream.write(f'{y} {len(zs)} ')
            for z in zs:
              edep = yslice[(x, y, z)]
              stream.write(f'{z} {edep} ')
        stream.write('\n')
      else:
        print(self._string, file=stream, end='')
    
    def get_etot(self):
      return self.data.etot
    
    def get_edep(self, xindex: Optional[int]|ellipsis_type = None, yindex: Optional[int] = None, zindex: Optional[int] = None):
      """Energy deposit in the element given by x, y, and z indices.
      1. get_edep() returns flat list of all edeps.
      2. get_edep(x, y, z) returns edep at item (x, y, z).
      3. get_edep(x) returns total edep at layer x.
      4. get_edep(...), i.e. with Elipsis, returns list of total edeps by layer."""
      if yindex is None and zindex is None:
      
        if xindex is None:  # case 1
          return [
            self.data.edep_dict[(x, y, z)]
            for x in range(self.nlayers)
            for y in range(self.nitemsY[x])
            for z in range(self.nitemsZ[x])
          ]
        
        else:
          if xindex is ...:   # case 4
            return [
              sum(
                self.data.edep_dict[(x, y, z)]
                for y in range(self.nitemsY[x])
                for z in range(self.nitemsZ[x])
              )
              for x in range(self.nlayers)
            ]
          else:  # case 3
            return sum(
              self.data.edep_dict[(xindex, y, z)]
              for y in range(self.nitemsY[xindex])
              for z in range(self.nitemsZ[xindex])
            )
      
      else:  # case 2
        assert(xindex is not None  and  yindex is not None  and  zindex is not None)
        # return self.data.edep[xindex][yindex][zindex]
        assert(xindex in range(self.nlayers)  and  yindex in range(self.nitemsY[xindex])  and  zindex in range(self.nitemsY[xindex]))
        return self.data.edep_dict[(xindex, yindex, zindex)]
    
    def is_triggered(self, threshold = 0) -> bool:
      return any(edep > threshold for edep in self.get_edep())
  
  
  class Data:
    pass
  
  system_types = {}
  
  @property
  def topAC(self): "ACSystem"; return self._AC
  system_types['topAC'] = ACSystem
  
  @property
  def leftAC(self): "ACSystem"; return self._leftAC
  system_types['leftAC'] = ACSystem
  
  @property
  def frontAC(self): "ACSystem"; return self._frontAC
  system_types['frontAC'] = ACSystem
  
  @property
  def rightAC(self): "ACSystem"; return self._rightAC
  system_types['rightAC'] = ACSystem
  
  @property
  def backAC(self): "ACSystem"; return self._backAC
  system_types['backAC'] = ACSystem
  
  @property
  def C(self): "StripSystem"; return self._C
  system_types['C'] = StripSystem
  
  @property
  def S1(self): "ToFSystem"; return self._S1
  system_types['S1'] = ToFSystem
  
  @property
  def S2(self): "ToFSystem"; return self._S2
  system_types['S2'] = ToFSystem
  
  @property
  def CC1(self): "StripSystem"; return self._CC1
  system_types['CC1'] = StripSystem
  
  @property
  def CC1calo(self): "CC1ScintSystem"; return self._CC1calo
  system_types['CC1calo'] = CC1ScintSystem
  
  @property
  def S3(self): "ScintSystem"; return self._S3
  system_types['S3'] = ScintSystem
  
  @property
  def leftLD(self): "LDSystem"; return self._leftLD
  system_types['leftLD'] = ScintSystem
  
  @property
  def frontLD(self): "LDSystem"; return self._frontLD
  system_types['frontLD'] = ScintSystem
  
  @property
  def rightLD(self): "LDSystem"; return self._rightLD
  system_types['rightLD'] = ScintSystem
  
  @property
  def backLD(self): "LDSystem"; return self._backLD
  system_types['backLD'] = ScintSystem
  
  @property
  def CC2(self): "CaloSystem"; return self._CC2
  system_types['CC2'] = CaloSystem
  
  @property
  def S4(self): "ScintSystem"; return self._S4
  system_types['S4'] = ScintSystem



class HarryPytherException(Exception):
  """Abstract HarryPyther Exception"""
  pass

class NoVersionException(HarryPytherException):
  def __init__(self, header):
    self.header = header
  def __str__(self):
    return f'Cannot determine version number; wrong header line:\n{self.header}'

class NotImplementedError(HarryPytherException):
  def __init__(self, header):
    self.header = header
  def __str__(self):
    return f'{self.header} not implemented'

class WrongFormatException(HarryPytherException):
  def __init__(self, msg):
    self.msg = msg
  def __str__(self):
    return self.msg


def write_string(stream: IO, *vals):
  for val in vals:
    stream.write(str(val))
    stream.write(' ')




def listmap(fun, *iterable):
  return list(map(fun, *iterable))

def cropline(line):
  return line if len(line) < 50 else line[:46] + '...'


# check versions
def Umbridge_or_higher(hp):
  return hp.version_major >= 17




def maps_append(values: list, *arrays):
  """Process values via given functions and append them to arrays
  Syntax:
    maps_append([v1, v2, ...], fcn1, list1, fcn2, list2, ...)
  Example:
    names = []; numbers = []; values = []; romans = []
    maps_append('one  1 1.00 I'.split(),  str,names, int,numbers, float,values, roman_to_int,romans)
    maps_append('nine 9 9.00 IX'.split(), str,names, int,numbers, float,values, roman_to_int,romans)
  """
  size = len(values)
  funcs = arrays[::2]
  if size*2 != len(arrays):
    raise WrongFormatException(
      f'Must be {len(arrays)//2} values: {listmap(lambda a: a.__name__,funcs)}.\nReceived {size}: {values}'
    )
  arrays = arrays[1::2]
  for i in range(size):
    arrays[i].append(funcs[i](values[i]))

def maps(values: list, *funcs):
  """Process values via given functions and return them as array
  Example:
    name, number, value, roman = maps('one 2 3.14 IV'.split(),  str, int, float, roman_to_int)
  """
  size = len(values)
  if size != len(funcs):
    raise WrongFormatException(
      f'Must be {len(funcs)} values: {listmap(lambda a: a.__name__,funcs)}.\nReceived {size}: {values}'
    )
  return [funcs[i](values[i]) for i in range(size)]


def len_stored(array: sparse):
  return len(array.indices())