#!/usr/bin/env python3

import collections

class sparse(collections.abc.Iterable):
  """Sparse (1D) array class.  An iterable that holds only non-default values.
  The default value is, by default, `None`.
  """
  def __init__(self, pairs = None, size = None, default = None):
    """Sparse array constructor.
    
    Usage:
        sparse(<non-sparse-list>, [<size>], [<default>])
            Convert <non-sparse-list> to a sparse array.
        
        sparse(<another-sparse-array>)
            Copy array.
        
        sparse(<another-sparse-array>, <size>, <default>)
            Copy array and change its size and dafault value.
        
        sparse(<index-value-dict>, <size>, <default>)
            Construct array using (its internal data):
                
                <index-value-dict>:  A dictionary mapping indices to values.
                                     Indices that are not specified
                                     would “contain” default values.
                
                <size>:  Size of the array.  If not specified, it is computed
                         so that the array includes the largest index
                         specified in <index-value-dict>.
                
                <default>:  A value assumed when requesting an index not explicitly specified.
    """
    if pairs is None:
      pairs = {}
    elif isinstance(pairs, sparse):
      from copy import copy
      self._pairs = copy(pairs._pairs)
      if size is None and default is None:
        self._size = pairs._size
        self._default = copy(pairs._default)
      else:
        self._size = size
        self._correct_size()
        self._default = default
      return
    elif isinstance(pairs, list):
      self._pairs = {}
      self._size = len(pairs) if size is None else size
      self._default = default
      for index in range(len(pairs)):
        el = pairs[index]
        if el != default:
          self._pairs[index] = el
      return
    if not isinstance(pairs, dict):
      raise TypeError('{} must be a dict, list, or sparse'.format(pairs))
    #for pair in pairs:
    #  if not isinstance(pair, tuple) or not len(pair) == 2 or not isinstance(pair[0], int):
    #    raise TypeError('{} must be a pair tuple'.format(pair))
    self._pairs = pairs
    self._size = size
    self._correct_size()
    self._default = default
  
  def _correct_size(self):
    if self._size is None or not isinstance(self._size, int):
      self._size = max(self.indices()) + 1 if self._pairs else 0
  
  def __repr__(self):
    if self._default:
      return 'sparse(' + repr(self._pairs) + ', ' + repr(self._size) + ', ' + repr(self._default) + ')'
    else:
      return 'sparse(' + repr(self._pairs) + ', ' + repr(self._size) + ')'
  
  def __str__(self):
    if self._size <= 6:
      return str(list(self))
    else:
      return '[{}, {}, {}, <{miss}>, {}, {}, {}]'.format(
        self[0], self[1], self[2], self[-3], self[-2], self[-1], miss = self._size - 6)
  
  def __iter__(self):
    self._iteri = -1
    self._indices = self.indices()
    return self
  
  def __next__(self):
    self._iteri += 1
    if self._iteri >= self._size:
      raise StopIteration()
    if self._iteri in self._indices:
      return self._pairs[self._iteri]
    else:
      return self._default
    
  
  def list(self):
    "Convert to a list."
    l = [self._default] * self._size
    for pair in self._pairs.items():
      l[pair[0]] = pair[1]
    return l
  
  def indices(self):
    "Get specified indices."
    return self._pairs.keys()
  
  def dict(self):
    "Get specified index-value ditcionary."
    return self._pairs
  
  def default(self):
    "Get default value."
    return self._default
  
  def __len__(self):
    "Get size of the array."
    return self._size
  
  def __getitem__(self, index):
    "Get an element."
    if not isinstance(index, int):
      raise TypeError('only integer indices supported'.format(size))
    if not -self._size <= index < self._size:
      raise IndexError('sparse index out of range')
    indices = self.indices()
    if index in indices or self._size + index in indices:
      if index < 0:
        return self._pairs[self._size + index]
      else:
        return self._pairs[index]
    else:
      return self._default
  
  def __setitem__(self, index, value):
    "Set an element.  index must be in range given by `size`."
    if not isinstance(index, int):
      raise TypeError('only integer indices supported'.format(size))
    if not -self._size <= index < self._size:
      raise IndexError('sparse index out of range')
    if index < 0:
      index = self._size + index
    if value is not self._default:
      self._pairs[index] = value
    else:
      if index in self._pairs:
        del self._pairs[index]
      else:
        self._pairs[index] = value
  
  def resize(self, size):
    """Set new size of the array.
    When shrinking, values at indices out of the new size are not cleared
    but cannot be accessed until the sized is large enough again."""
    if not isinstance(size, int) or size < 0:
      raise TypeError('{} must be a non-negative int'.format(size))
    self._size = size
  
  def clear(self):
    "Clear data.  The size and default value is not affected."
    self._pairs = {}
  
  def clone(self):
    "Return a copy of the array.  Data dictionary and the default value are cloned."
    from copy import copy
    return sparse(copy(self._pairs), self._size, copy(self._default))
  
  def MMASparse(self):
    "Wolfram language SparseArray representation."
    d = self.dict()
    return ( 'SparseArray[{' +
      ', '.join('{} -> {}'.format(k, d[k]) for k in d) +
      '}}, {}, {}]'.format(len(self), self.default()) )

if __name__ == '__main__':
  s = sparse([0,2,0,0,8,0,0,8], default = 0)
  print('s = sparse([0,2,0,0,8,0,0,8], default = 0)')
  print(repr(s))
  print(list(s))
  print(s.MMASparse())
  s[-2] = 1
  print('s[-2] = 1')
  print(repr(s))
  print(list(s))
  s.resize(19)
  print('s.resize(19)')
  print(repr(s))
  print(list(s))
