#!/usr/bin/env python3

class struct(object):
  """Struct object.
  In essence, like a string-key dict with an attribute syntax to get/set elements.
  Usage:
      s = struct(one = 1, two = 2)
      # or
      s = struct({'one': 1, 'two': 2})
      
      assert(s.one == 1 and s.two == 2)
  """
  
  def __init__(self, d = None, recursive = False, **dd):
    if d is None:
      d = dd
    if isinstance(d, dict):
      for a, b in d.items():
        if isinstance(b, list):
          setattr(self, a, [struct(x, recursive = recursive) if recursive and isinstance(x, dict) else x for x in b])
        elif isinstance(b, tuple):
          setattr(self, a, tuple(struct(x, recursive = recursive) if recursive and isinstance(x, dict) else x for x in b))
        else:
          setattr(self, a, struct(b, recursive = recursive) if recursive and isinstance(b, dict) else b)
    else:
      setattr(self, "unnamed key", d)
  
  def __repr__(self):
    return 'struct({})'.format(self.__dict__)
