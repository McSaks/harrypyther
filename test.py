#!/usr/bin/env python3


def usage(stream = None):
  import sys
  print ( '{cmd} [ -v VERS | --file-version VERS ] [ -h | --help ] FILE' . format(cmd = sys.argv[0]) ,
    file = sys.stdout if stream is None else stream )

def process_parameters():
  import getopt, sys
  try:
    opts, args = getopt.getopt(
        sys.argv[1:],
        'v:h',
        ['file-version=', 'help']
      )
  except getopt.GetoptError as err:
    print(str(err)) # will print something like "option -a not recognized"
    usage(sys.stderr)
    sys.exit(2)
  optmap = {}
  for k, v in opts:
    if False: pass
    elif k in ("-v", "--file-version"):
      optmap['file-version'] = v
    elif k in ("-h", "--help"):
      usage()
      sys.exit(0)
    else:
      assert False, "unhandled option"
  if len(args) > 1:
    usage(sys.stderr)
    sys.exit(2)
  optmap['file to process'] = args[0] if len(args) >= 1 else 'default.eventdata'
  return optmap

def main():
  import sys
  # sys.path.append('..')
  optmap = process_parameters()
  print(optmap)
  try:
    with open(optmap['file to process']) as file:
      import re
      header = file.readline()
      version = header.lstrip('# Created by ').strip()
      search = re.search(r'\bv([\d.]+)\b', version)
      if search is None:
        print(
          'Error: cannot determine version number; wrong header line:', header,
          sep = '\n',
          file = sys.stderr)
        sys.exit(4)
      version_number = search.group(1)
      print(version, version_number, sep = '\n')
  except FileNotFoundError as err:
    print('Error:', str(err), file = sys.stderr)
    sys.exit(3)


if __name__ == "__main__":
  main()
